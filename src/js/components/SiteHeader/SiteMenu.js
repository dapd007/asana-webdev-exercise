import React, { Component } from 'react';

class SiteMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showMenu: false
        };
    }

    toggleMenu() {
        this.setState({
            showMenu: !this.state.showMenu
        });
    }

    render() {
        return (
            <div className="site-header-nav-wrapper">
                <nav className={"site-header-nav" + (this.state.showMenu ? ' is-open' : '')}>
                    <a href="/">About Us</a>
                </nav>
                <div className="site-header-mobile">
                    <a href="javascript:" className={"nav-icon"  + (this.state.showMenu ? ' is-open' : '')} onClick={this.toggleMenu.bind(this)}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
            </div>
        )
    }

}

export default SiteMenu;
